package it.polito.dp2.FDS.lab1.tests;

import it.polito.dp2.FDS.FlightMonitorException;
import it.polito.dp2.FDS.MalformedArgumentException;

import java.io.File;
import java.io.FileNotFoundException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

public abstract class TestsAbstract extends it.polito.dp2.FDS.tests.TestsAbstract {
	private final static int assignmentNumber = 1;
	
	protected int getAssignmentNumber() {
		return assignmentNumber;
	}
	
	protected void runSerializerImpl(String[] args) throws FileNotFoundException, FlightMonitorException, MalformedArgumentException, ParserConfigurationException, TransformerException {
		it.polito.dp2.FDS.sol1.FDSInfoSerializer.main(args);
	}
	
	protected String getParserClass() {
		return "it.polito.dp2.FDS.sol1.FlightMonitorFactory";
	}
	
	protected String getFlightInfoPropertyKey() {
		return "it.polito.dp2.FDS.sol1.FlightInfo.file";
	}
	
	protected File getSchemaFile() {
		return new File("./dtd/flightInfo.dtd");
	}
}
