package it.polito.dp2.FDS.sol1;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Set;

import org.w3c.dom.*;

import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import it.polito.dp2.FDS.Aircraft;
import it.polito.dp2.FDS.FlightInstanceReader;
import it.polito.dp2.FDS.FlightMonitor;
import it.polito.dp2.FDS.FlightMonitorException;
import it.polito.dp2.FDS.FlightMonitorFactory;
import it.polito.dp2.FDS.FlightReader;
import it.polito.dp2.FDS.MalformedArgumentException;
import it.polito.dp2.FDS.PassengerReader;

public class FDSInfoSerializer {
	private FlightMonitor monitor;
	protected Document doc;	// document element
	protected Element root;	// document root element
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	
	/**
	 * @category Constructor
	 * @throws FlightMonitorException
	 */
	public FDSInfoSerializer() throws FlightMonitorException {
		FlightMonitorFactory factory = FlightMonitorFactory.newInstance();
		monitor = factory.newFlightMonitor();
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		// factory.setNamespaceAware (true);
		DocumentBuilder builder;
		try {
			builder = docFactory.newDocumentBuilder();
			// Create the document
			doc = builder.newDocument();
			// Create and append the root element
			root = (Element) doc.createElement("solFlightMonitor");
			doc.appendChild(root);
		} catch (ParserConfigurationException e) {
			// Something wrong with the file
			e.printStackTrace();
			throw new FlightMonitorException();
		}
	}
	
	/**
	 * @category Constructor
	 * @param monitor
	 */
	public FDSInfoSerializer(FlightMonitor monitor){
		this.monitor = monitor;
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		// factory.setNamespaceAware (true);
		DocumentBuilder builder;
		try {
			builder = docFactory.newDocumentBuilder();
			// Create the document
			doc = builder.newDocument();
			// Create and append the root element
			root = (Element) doc.createElement("solFlightMonitor");
			doc.appendChild(root);
		} catch (ParserConfigurationException e) {
			// Something wrong with the file
			e.printStackTrace();
		}
	}
	
	/**
	 * @category Printer
	 * @param out
	 * @throws TransformerException
	 */
	public void serialize(PrintStream out) throws TransformerException {
		TransformerFactory xformFactory = TransformerFactory.newInstance ();
		Transformer idTransform;
		idTransform = xformFactory.newTransformer ();
		idTransform.setOutputProperty(OutputKeys.INDENT, "yes");
		idTransform.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM , "flightInfo.dtd");
		idTransform.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
		Source input = new DOMSource (doc);
		Result output = new StreamResult (out);
		idTransform.transform (input, output);
	}

	/**
	 * @category Printer
	 */
	public void printAll() {
		printAircrafts();
		printFlights();
		printFlightInstances();
	}
	
	/**
	 * @category Printer
	 */
	private void printFlightInstances() {
		List<FlightInstanceReader> l;
		try {
			l = monitor.getFlightInstances(null, null, null);
			for (FlightInstanceReader f:l) {
				// get flight info in a StringBuffer
				Element xml_flightInstance = doc.createElement("flightInstance");
				root.appendChild(xml_flightInstance);
				xml_flightInstance.setAttribute("flightNumber", f.getFlight().getNumber());
				if(f.getAircraft()!=null)xml_flightInstance.setAttribute("aircraftId", f.getAircraft().model);
				xml_flightInstance.setAttribute("status", f.getStatus().toString());
				
			//region date	add the date
				Element el = doc.createElement("deptDate");
				xml_flightInstance.appendChild(el);
				GregorianCalendar date = f.getDate();
				int offset = date.get(Calendar.ZONE_OFFSET)/ (60*60*1000);
				String offsetString;
				if(offset == 0) offsetString = "Z";
				else offsetString =  offset > 0 ? 
						"+" + new DecimalFormat("00").format(offset) +":00" :
						""+ new DecimalFormat("00").format(offset) +":00";
				el.appendChild(doc.createTextNode(dateFormat.format(date.getTime()) + offsetString));
				
			// add delay and gate
				if(f.getDepartureGate() != null){
					el = doc.createElement("gate");
					el.appendChild(doc.createTextNode(f.getDepartureGate()));
					xml_flightInstance.appendChild(el);
				}
				el = doc.createElement("delay");
				el.appendChild(doc.createTextNode(""+f.getDelay()));
				xml_flightInstance.appendChild(el);
				
			// add passenger list
				Set<PassengerReader> set = f.getPassengerReaders(null);
				for (PassengerReader p:set){
					Element xml_passenger = doc.createElement("passenger");
					xml_flightInstance.appendChild(xml_passenger);
					xml_passenger.setAttribute("name", p.getName());
					xml_passenger.setAttribute("boarded", p.boarded()? "true":"false");
					xml_passenger.setIdAttribute("name", true);
					if(p.getSeat() != null){
						el = doc.createElement("seat");
						el.appendChild(doc.createTextNode(p.getSeat()));
						xml_passenger.appendChild(el);
					}
				}
			}
			
		} catch (MalformedArgumentException e) {
			// this exception will never be thrown because getFlightInstances is called with null arguments
			System.err.println("Unexpected exception");
			e.printStackTrace();
		}
	}

	/**
	 * @category Printer
	 */
	private void printFlights() {
		
		try {
			List<FlightReader> l = monitor.getFlights(null, null, null);
			for (FlightReader f:l) {
				printFlight(f);
			}
		} catch (MalformedArgumentException e) {
			// this exception will never be thrown because getFlights is called with null arguments
			System.err.println("Unexpected exception");
			e.printStackTrace();
		}
		
	}

	/**
	 * @category Printer
	 */
	private void printFlight(FlightReader f) {
		Element xml_flight = doc.createElement("flight");
		root.appendChild(xml_flight);
		xml_flight.setAttribute("number", f.getNumber());
		xml_flight.setIdAttribute("number", true);
		Element el = doc.createElement("deptIATAcode");
		el.appendChild(doc.createTextNode(f.getDepartureAirport()));
		xml_flight.appendChild(el);
		el = doc.createElement("destIATAcode");
		el.appendChild(doc.createTextNode(f.getDestinationAirport()));
		xml_flight.appendChild(el);

		Element xml_time = doc.createElement("deptTime");
		xml_time.appendChild(doc.createTextNode(new DecimalFormat("00").format(f.getDepartureTime().getHour())+":"+
												new DecimalFormat("00").format(f.getDepartureTime().getMinute())+":00"));
		xml_flight.appendChild(xml_time);
	}

	/**
	 * @category Printer
	 */
	private void printAircrafts() {
		Set<Aircraft> s = monitor.getAircrafts();
		for (Aircraft aircraft : s) {
			Element xml_aircraft = doc.createElement("aircraft");
			root.appendChild(xml_aircraft);
			
			Attr attr = doc.createAttribute("model");
			attr.setValue( aircraft.model);
			xml_aircraft.setAttributeNode(attr);

			for (String seat : aircraft.seats) {
				Element xml_seat = doc.createElement("seat");
				xml_seat.appendChild(doc.createTextNode(seat));
				xml_aircraft.appendChild(xml_seat);
			}
		}
	}
	
	/**
	 * @category main
	 * @param args
	 */
	public static void main(String[] args) {
		FDSInfoSerializer f;
		
		try {
			if(args.length != 1){
				System.out.println("Error: usage FDSInfoSerializer filePath");
			}
			f = new FDSInfoSerializer();
			f.printAll();
			f.serialize(new PrintStream(new File(args[0])));
		} catch (FlightMonitorException e) {
			System.err.println("Could not instantiate flight monitor object");
			e.printStackTrace();
			System.exit(1);
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
			System.exit(1);
		} catch (TransformerException e) {
			e.printStackTrace();
			System.exit(1);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

}
