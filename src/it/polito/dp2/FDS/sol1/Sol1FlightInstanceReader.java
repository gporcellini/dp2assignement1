package it.polito.dp2.FDS.sol1;

import it.polito.dp2.FDS.Aircraft;
import it.polito.dp2.FDS.FlightInstanceStatus;
import it.polito.dp2.FDS.FlightMonitorException;
import it.polito.dp2.FDS.FlightReader;
import it.polito.dp2.FDS.MalformedArgumentException;
import it.polito.dp2.FDS.PassengerReader;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Set;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class Sol1FlightInstanceReader implements it.polito.dp2.FDS.FlightInstanceReader{
	private Aircraft _aircraft;
	private FlightReader _flightReader;
	private GregorianCalendar _deptDate;
	private int _delay;
	private String _gate;
	private Set<PassengerReader> _passengers;
	private Set<String> _freeSeats;
	private FlightInstanceStatus _status;
	private final static Pattern pattern = Pattern.compile("^([0-9]{4})[-]([0-9]{2})[-]([0-9]{2})([+-][0-9]{2})[:][0]{2}$");
	private final static Pattern utcPattern= Pattern.compile("^([0-9]{4})[-]([0-9]{2})[-]([0-9]{2})[Z]$");

	//Constructors
	public Sol1FlightInstanceReader(
			Element xml_flightInstance, 
			Sol1FlightMonitor caller) 
			throws FlightMonitorException {
		super();
		_passengers = new HashSet<PassengerReader>();
		//FlightReader
		String flightNumber = xml_flightInstance.getAttribute("flightNumber");
		if(!it.polito.dp2.FDS.sol1.Sol1FlightReader.ValidateFlightNumber(flightNumber))
			throw new FlightMonitorException("Invalid flight number" + flightNumber);
		try {
			FlightReader flight = caller.getFlight(flightNumber);
			if(flight == null)
				throw new FlightMonitorException("Invalid flight number" + flightNumber);
			_flightReader = flight;
		} catch (MalformedArgumentException e1) {
			throw new FlightMonitorException("Invalid flight number" + flightNumber);
		}

		
		//Aircraft
		String aircraftId = xml_flightInstance.getAttribute("aircraftId");
		if(aircraftId.equals("")) _aircraft = null; //no aircraft assigned to flight instance yet
		else{
			try {
					Aircraft aircraft = caller.getAircraft(aircraftId);
					if(aircraft == null)
						throw new FlightMonitorException("Invalid aircraft model" + aircraftId);
					_aircraft = aircraft;
					_freeSeats = new HashSet<String>(_aircraft.seats);
			} catch (MalformedArgumentException e1) {
				throw new FlightMonitorException("Invalid aircraft model" + aircraftId);
			}
		}

		
		//Status
		FlightInstanceStatus status = StringToFlightInstanceStatus(xml_flightInstance.getAttribute("status"));
		if(status == null)
			throw new FlightMonitorException("Invalid status for flight instance");
		_status = status;
		
		//deptDate
		NodeList nodeList = xml_flightInstance.getElementsByTagName("deptDate");
		_deptDate = FlightInfoDeptDateToGregorianCalendar((Element)nodeList.item(0));
		
		//gate
		nodeList = xml_flightInstance.getElementsByTagName("gate");
		if(nodeList.getLength() == 0)
			_gate = null; //no gate defined yet
		else{
			Node node = nodeList.item(0).getFirstChild();
			if(node == null)
				throw new FlightMonitorException("Invalid gate value in flightInstance, nullref");
			_gate = node.getNodeValue();
		}
		
		//delay
		nodeList = xml_flightInstance.getElementsByTagName("delay");
		try {
			Node node = nodeList.item(0).getFirstChild();
			if(node == null)
				throw new FlightMonitorException("Invalid delay value in flightInstance, nullref");
			_delay = Integer.parseInt(node.getNodeValue());
		}
		catch (NumberFormatException e){
			throw new FlightMonitorException("Invalid delay value for flightInstance");
		}
		
		//passengers
		nodeList = xml_flightInstance.getElementsByTagName("passenger");
		LoadPassengers(nodeList);
	}

	/**
	 * 
	 * @param nodeList
	 * @param aircraft
	 * @throws MalformedArgumentException in case seat is not valid for aircraft o aircraft is null
	 */
	private void LoadPassengers(NodeList nodeList) throws FlightMonitorException {
		Element xml_passenger;
		Set<String> nameSet = new HashSet<String>();
		for (int i = 0; i < nodeList.getLength(); i++){
			xml_passenger = (Element)nodeList.item(i);
			String name = xml_passenger.getAttribute("name");
			if(nameSet.contains(name))
				throw new FlightMonitorException("Two passengers with the same name (" + name +") on the same flightInstance");
			nameSet.add(name);
			String boardedString = xml_passenger.getAttribute("boarded");
			boolean boarded = boardedString.equals("true") ? true : false;
			NodeList xml_seat = xml_passenger.getElementsByTagName("seat");
			String seat;
			if(xml_seat.getLength() == 0) {
				if(boarded)
					throw new FlightMonitorException("Passenger " + name + " is boarded but has no assigned seat");
				else 
					seat = null;
			}
			else {
				if(_aircraft == null)
					throw new FlightMonitorException("No aircraft yet assigned to flightIstance, passenger " + name 
							+" couldn't have any assigned seat");
				Node node =xml_seat.item(0).getFirstChild();
				if(node == null)
					throw new FlightMonitorException("Invalid seat value for passenger"+ name +", null ref");
				seat = node.getNodeValue();
				if(!ValidateSeat(seat)) 
					throw new FlightMonitorException("Seat of passenger " + name + " is invalid: "
							+ "no such seat for specified aircraft or seat already assigned to another passenger");
				_freeSeats.remove(seat);
			}
		
			_passengers.add(new it.polito.dp2.FDS.sol1.Sol1PassengerReader(name, boarded, this, seat));
		}
		
	}
	

	//Converters && validators
	/**
	 * 
	 * @param xml_date
	 * @return
	 * @throws MalformedArgumentException
	 */
	public static GregorianCalendar FlightInfoDeptDateToGregorianCalendar(Element xml_date) throws FlightMonitorException{
		try{
			String dateString = xml_date.getFirstChild().getNodeValue();
			int timeOffset = 0;
			
			Matcher matcher = utcPattern.matcher(dateString);
			if(!matcher.matches()){
				matcher = pattern.matcher(dateString);
				if(!matcher.matches())
					throw new FlightMonitorException("Invalid deptDate");
				timeOffset = Integer.parseInt(matcher.group(4));
			}
			
			int year = Integer.parseInt(matcher.group(1));
			int month = Integer.parseInt(matcher.group(2)) - 1;
			int day = Integer.parseInt(matcher.group(3));

			return ValidDate(day, month, year,  timeOffset);
		}
		catch (NullPointerException npe) {
			throw new FlightMonitorException("Invalid deptDate");
		}
		catch (NumberFormatException e) {
			throw new FlightMonitorException("Invalid deptDate");
		}
	}
	
	/**
	 * @param dd
	 * @param mm
	 * @param yyyy
	 * @param timeOffset
	 * @return Valid GregorianCalendarInstance
	 * @throws MalformedArgumentException in case arguments are not valid
	 */
	private static GregorianCalendar ValidDate(int dd, int mm, int yyyy, int timeOffset) throws FlightMonitorException{
		if(timeOffset < -12 || timeOffset > 12 || mm < 0 || mm > 11 || yyyy < 1970)
			throw new FlightMonitorException("Invalid deptDate");
		
	    GregorianCalendar date = new GregorianCalendar();
	    date.clear();
	    date.set(Calendar.ZONE_OFFSET, timeOffset);
	    date.set(Calendar.YEAR, yyyy);
	    date.set(Calendar.MONTH, mm);
	    if(dd > date.getActualMaximum(Calendar.DAY_OF_MONTH) || dd < date.getActualMinimum(Calendar.DAY_OF_MONTH))
	    	throw new FlightMonitorException("Invalid deptDate");
	    date.set(Calendar.DAY_OF_MONTH, dd);
	    
	    return date;
	}
	
	private boolean ValidateSeat(String seat) {
		if(_freeSeats.contains(seat)) return true;
		return false;
	}
	
	/**
	 * 
	 * @param str
	 * @return
	 */
	public static FlightInstanceStatus StringToFlightInstanceStatus(String str){
		if(str.equals("ARRIVED")) return FlightInstanceStatus.ARRIVED;
		else if(str.equals("BOARDING")) return FlightInstanceStatus.BOARDING;
		else if(str.equals("BOOKING")) return FlightInstanceStatus.BOOKING;
		else if(str.equals("CANCELLED")) return FlightInstanceStatus.CANCELLED;
		else if(str.equals("CHECKINGIN")) return FlightInstanceStatus.CHECKINGIN;
		else if(str.equals("DEPARTED")) return FlightInstanceStatus.DEPARTED;
		
		return null;
	}
	
	//Getters
	@Override
	public Aircraft getAircraft() {
		return _aircraft;
	}

	@Override
	public GregorianCalendar getDate() {
		return _deptDate;
	}

	@Override
	public int getDelay() {
		return _delay;
	}

	@Override
	public String getDepartureGate() {
		return _gate;
	}

	@Override
	public FlightReader getFlight() {
		return _flightReader;
	}

	@Override
	public Set<PassengerReader> getPassengerReaders(String arg0) {
		if(arg0 == null)
			return _passengers;
		Set<PassengerReader> subSet = new HashSet<PassengerReader>();
		for(PassengerReader passengerReader : _passengers){
			if(passengerReader.getName().toLowerCase().startsWith(arg0.toLowerCase()))
				subSet.add(passengerReader);
		}
		return subSet;
	}

	@Override
	public FlightInstanceStatus getStatus() {
		return _status;
	}

}
