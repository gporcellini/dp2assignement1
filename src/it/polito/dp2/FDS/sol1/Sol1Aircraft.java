package it.polito.dp2.FDS.sol1;

import it.polito.dp2.FDS.FlightMonitorException;

import java.util.HashSet;
import java.util.Set;

import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Sol1Aircraft extends it.polito.dp2.FDS.Aircraft{

	public Sol1Aircraft(String model, Set<String> seats) {
		super(model, seats);
	}
	
	public Sol1Aircraft(Element xml_aircraft) throws FlightMonitorException{
		super("", new HashSet<String>());
		this.model = xml_aircraft.getAttribute("model");
		NodeList xml_seats = xml_aircraft.getElementsByTagName("seat");
		
		Element xml_seat;
		for(int i = 0; i < xml_seats.getLength(); i++){
			xml_seat = (Element)xml_seats.item(i);
			try{
				Node node = xml_seat.getFirstChild();
				if(node == null) 
					throw new FlightMonitorException("Invalid XMLFile entry, null ref, aircraft model " + this.model);
				if(!this.seats.contains(node.getNodeValue()))
					this.seats.add(node.getNodeValue());
				else
					throw new FlightMonitorException("Invalid XMLFile entry, more than one seat with the same code"
							+ ", aircraft model " + this.model);
			}
			catch(DOMException e){
				throw new FlightMonitorException("Invalid XMLFile entry, DOMexception, aircraft model " + this.model);
			}
		}
	}
}
