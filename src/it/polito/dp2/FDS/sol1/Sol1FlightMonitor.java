package it.polito.dp2.FDS.sol1;

import it.polito.dp2.FDS.Aircraft;
import it.polito.dp2.FDS.FlightInstanceReader;
import it.polito.dp2.FDS.FlightInstanceStatus;
import it.polito.dp2.FDS.FlightMonitorException;
import it.polito.dp2.FDS.FlightReader;
import it.polito.dp2.FDS.MalformedArgumentException;
import it.polito.dp2.FDS.Time;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;


public class Sol1FlightMonitor implements it.polito.dp2.FDS.FlightMonitor {
	private HashMap<String, Aircraft> _aircrafts;
	private HashMap<String, SortedMap<GregorianCalendar, FlightInstanceReader>> _flightInstances; 
	private HashMap<String, FlightReader> _flights;
	private static Sol1FlightMonitor _instance;
	Comparator<GregorianCalendar> customComparator = new Comparator<GregorianCalendar>() {
		@Override
		public int compare(GregorianCalendar o1, GregorianCalendar o2) {
			if(o1.get(Calendar.YEAR) == o2.get(Calendar.YEAR)){
				if(o1.get(Calendar.MONTH) == o2.get(Calendar.MONTH)){
					if(o1.get(Calendar.DAY_OF_MONTH) == o2.get(Calendar.DAY_OF_MONTH)){
						return 0;
					}else{
						return Integer.compare(o1.get(Calendar.DAY_OF_MONTH), o2.get(Calendar.DAY_OF_MONTH));
					}
				}else{
					return Integer.compare(o1.get(Calendar.MONTH), o2.get(Calendar.MONTH));
				}	
			}else{
				return Integer.compare(o1.get(Calendar.YEAR),o2.get(Calendar.YEAR));
			}
		}
	};
	
	
	public static Sol1FlightMonitor getInstance(){
		if(_instance == null) _instance = new Sol1FlightMonitor();
		
		return _instance;
	}
	
    protected void parse(String filename) throws FlightMonitorException
    {
    	if(filename == null) {
    		System.out.println("Filename was null");
    		throw new FlightMonitorException();
    	}
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setValidating(true);   
        	DocumentBuilder builder = factory.newDocumentBuilder();
        	builder.setErrorHandler(new DomParseVSHandler());
        	Document document = builder.parse( new File(filename) );

        	LoadAll(document);
        }
        catch (IOException ioe) {
            System.out.println("Input/Output error: " + ioe.getMessage());
            throw new FlightMonitorException();
        }
        catch (SAXParseException spe) {
            System.out.println("Parsing error in XML file " + spe.getSystemId());
            System.out.println(" at line: " + spe.getLineNumber() +
                " column: " + spe.getColumnNumber());
            System.out.println(spe.getMessage() );
            throw new FlightMonitorException();
        }
        catch (SAXException se) {
            System.out.println("General SAX exception: " + se.getMessage());
            throw new FlightMonitorException();
        }
        catch (ParserConfigurationException pce) {
            System.out.println("Parser configuration exception: " + pce.getMessage());
            throw new FlightMonitorException();
        }
    }
    
	private Sol1FlightMonitor(){
		_aircrafts = new HashMap<String, Aircraft>();
		_flightInstances = new HashMap<String, SortedMap<GregorianCalendar, FlightInstanceReader>>();
		_flights = new HashMap<String, FlightReader>();
	}
	
	//Loaders
	private void LoadAll(Document document) throws FlightMonitorException{
    	LoadFlights(document);
    	LoadAircrafts(document);
    	LoadFlightInstances(document);
	}
	
	private void LoadFlights(Document document) throws FlightMonitorException {
		if(document == null) throw new FlightMonitorException();
		
		NodeList xml_flights = document.getElementsByTagName("flight");
		
		Element element;
		for(int i = 0; i < xml_flights.getLength(); i++){
			element = (Element) xml_flights.item(i);
			if(element != null){
				FlightReader tmp = new it.polito.dp2.FDS.sol1.Sol1FlightReader(element);
				_flights.put(tmp.getNumber(), tmp);
			}
			else 
				throw new FlightMonitorException("LoadFlights: null ref, wrong format of file");
		}
	}

	private void LoadFlightInstances(Document document) throws FlightMonitorException {
		if(document == null) throw new FlightMonitorException();
		
		NodeList xml_flightInstances = document.getElementsByTagName("flightInstance");
		
		Element element;
		for(int i = 0; i < xml_flightInstances.getLength(); i++){
			element = (Element) xml_flightInstances.item(i);
			if(element != null){
				FlightInstanceReader tmp = new it.polito.dp2.FDS.sol1.Sol1FlightInstanceReader(element, this);
				if(!_flightInstances.containsKey(tmp.getFlight().getNumber())){
					_flightInstances.put(tmp.getFlight().getNumber(), 
							new TreeMap<GregorianCalendar, FlightInstanceReader>(customComparator));
				}
				if(_flightInstances.get(tmp.getFlight().getNumber()).containsKey(tmp.getDate())){
					throw new FlightMonitorException("LoadFlightInstances:"
							+ " more than one flightInstance with the same date & flightNumber");
				}
					
				_flightInstances.get(tmp.getFlight().getNumber()).put(tmp.getDate(), tmp);
			}
			else
				throw new FlightMonitorException("LoadFlightInstances: null ref, wrong format of file");
		}
	}

	private void LoadAircrafts(Document document) throws FlightMonitorException {
		if(document == null) throw new FlightMonitorException();
		
		NodeList xml_aircrafts = document.getElementsByTagName("aircraft");
		
		Element element;
		for(int i = 0; i < xml_aircrafts.getLength(); i++){
			element = (Element) xml_aircrafts.item(i);
			if(element != null){
				Aircraft tmp = new it.polito.dp2.FDS.sol1.Sol1Aircraft(element);
				_aircrafts.put(tmp.model, tmp);
			}
			else
				throw new FlightMonitorException("LoadAicrafts: null ref, wrong format of file");
		}
	}

	
	//GETTERS
	
	/**
	 * @category Getter
	 */
	@Override
	public Set<Aircraft> getAircrafts() {
		return new HashSet<Aircraft>(_aircrafts.values());
	}
	
	/**
	 * @category Getter
	 */
	protected Aircraft getAircraft(String model) throws MalformedArgumentException{
		if(model == null) throw new MalformedArgumentException();
		
		if(_aircrafts.containsKey(model)) return _aircrafts.get(model);
		
		return null;
	}

	/**
	 * @category Getter
	 */
	@Override
	public FlightReader getFlight(String arg0)
			throws MalformedArgumentException { //tested
		if(arg0 == null || !ValidateFlightNumber(arg0)){
			throw new MalformedArgumentException();
		}
		
		if(_flights.containsKey(arg0)) return _flights.get(arg0);
		
		return null;
	}

	/**
	 * @category Getter
	 */
	@Override
	public FlightInstanceReader getFlightInstance(String arg0,
			GregorianCalendar arg1) throws MalformedArgumentException { //tested
		if(arg0 == null || arg1==null || !ValidateFlightNumber(arg0))
			throw new MalformedArgumentException();
		
		if(_flightInstances.containsKey(arg0)) return _flightInstances.get(arg0).get(arg1);
		
		return null;
	}

	/**
	 * @category Getter
	 */
	@Override
	public List<FlightInstanceReader> getFlightInstances(String arg0,
			GregorianCalendar arg1, FlightInstanceStatus arg2)
			throws MalformedArgumentException { //tested
		if(arg0 != null && !ValidateFlightNumber(arg0))
			throw new MalformedArgumentException();
		
		List<FlightInstanceReader> list = new ArrayList<FlightInstanceReader>();
		if(arg0 == null){
			for(SortedMap<GregorianCalendar, FlightInstanceReader> subMap : _flightInstances.values()){
				list.addAll(FlightInstanceSubList(subMap, arg1, arg2));
			}
		}else if(_flightInstances.containsKey(arg0)){
			SortedMap<GregorianCalendar, FlightInstanceReader> subMap = _flightInstances.get(arg0);
			list = FlightInstanceSubList(subMap, arg1, arg2);
		}
		
		return list;
	}
	
	/**
	 * @category Getter
	 */
	private List<FlightInstanceReader> FlightInstanceSubList(SortedMap<GregorianCalendar, FlightInstanceReader> subMap, 
			GregorianCalendar arg1,
			FlightInstanceStatus arg2){
		List<FlightInstanceReader> list = new ArrayList<FlightInstanceReader>();

		if(subMap.size() == 0) return list;
		if(arg1 != null) {
			if(customComparator.compare(arg1, subMap.lastKey()) > 0) return list;
			else subMap = subMap.tailMap(arg1);
		}
		
		for (FlightInstanceReader fir : subMap.values()){
			if(arg2 == null || fir.getStatus() == arg2){
				list.add(fir);
			}
		}
		
		return list;
	}

	/**
	 * @category Getter
	 */
	@Override
	public List<FlightReader> getFlights(String dep, String arr, Time time)
			throws MalformedArgumentException {
		if((dep != null && !ValidateIATACode(dep)) || 
				(arr != null && !ValidateIATACode(arr)) )
			throw new MalformedArgumentException();
		List<FlightReader> list = new ArrayList<FlightReader>();
		
		for(FlightReader flightReader : _flights.values())
			if(CheckFlight(flightReader, dep, arr, time)) list.add(flightReader);
		
		return list;
	
	}
	
	
	//VALIDATORS
	
	/**
	 * @category Validator	
	 * @param flightReader
	 * @param dep
	 * @param arr
	 * @param time
	 * @return
	 */
	private boolean CheckFlight(FlightReader flightReader, String dep, String arr, Time time){
		if(dep == null) return CheckFlightArr(flightReader, arr, time);
		else if(dep.equals(flightReader.getDepartureAirport()))	return CheckFlightArr(flightReader, arr, time);
		
		return false;
	}
	
	/**
	 * @category Validator
	 * @param flightReader
	 * @param arr
	 * @param time
	 * @return
	 */
	private boolean CheckFlightArr(FlightReader flightReader, String arr, Time time){
		if(arr == null) return CheckFlightTime(flightReader, time);
		else if(arr.equals(flightReader.getDestinationAirport())) return CheckFlightTime(flightReader, time);
		
		return false;
	}
	
	/**
	 * @category Validator
	 * @param flightReader
	 * @param time
	 * @return
	 */
	private boolean CheckFlightTime(FlightReader flightReader, Time time){
		if(time == null) return true;
		else if(!flightReader.getDepartureTime().precedes(time)) return true;
		
		return false;
	}
	
	/**
	 * @category Validator
	 * @param IATAcode
	 * @return
	 */
	public static boolean ValidateIATACode(String IATAcode){
		if(IATAcode.matches("^[A-Za-z]{3}$")) return true;
		else return false;
	}

	/**
	 * @category Validator
	 * @param flightNumber
	 * @return
	 */
	public static boolean ValidateFlightNumber(String flightNumber){
		if(flightNumber.matches("^[A-Za-z]{2}[0-9]{1,4}$")) return true;
		else return false;
	}

}
	


class DomParseVSHandler extends org.xml.sax.helpers.DefaultHandler {

	  // Validation errors are treated as fatal
	  public void error (SAXParseException e) throws SAXParseException
	  {
	    throw e;
	  }

	  // Warnings are displayed (without terminating)
	  public void warning (SAXParseException e) throws SAXParseException
	  {
	    System.out.println("** Warning"
	            + ", file " + e.getSystemId()
	            + ", line " + e.getLineNumber());
	    System.out.println("   " + e.getMessage() );
	  }
}
