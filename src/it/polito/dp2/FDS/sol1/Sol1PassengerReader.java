package it.polito.dp2.FDS.sol1;

import it.polito.dp2.FDS.FlightInstanceReader;

public class Sol1PassengerReader implements it.polito.dp2.FDS.PassengerReader{
	private boolean _boarded;
	private String _name;
	private FlightInstanceReader _flightInstance;
	private String _seat;

	//Constructors
	public Sol1PassengerReader(String name, boolean boarded, FlightInstanceReader flightInstance, String seat) {
		_name = name;
		_boarded = boarded;
		_flightInstance = flightInstance;
		_seat = seat;
	}
	
	//getters
	@Override
	public boolean boarded() {
		return _boarded;
	}

	@Override
	public FlightInstanceReader getFlightInstance() {
		return _flightInstance;
	}

	@Override
	public String getName() {
		return _name;
	}

	@Override
	public String getSeat() {
		return _seat;
	}

}
