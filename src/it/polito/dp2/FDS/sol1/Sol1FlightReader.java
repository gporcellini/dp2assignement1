package it.polito.dp2.FDS.sol1;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import it.polito.dp2.FDS.FlightMonitorException;
import it.polito.dp2.FDS.Time;

public class Sol1FlightReader implements it.polito.dp2.FDS.FlightReader{
	private String number;
	private Time deptTime;
	private	String deptIATAcode;
	private String destIATAcode;
	private static final Pattern timePattern = Pattern.compile("^([0-2][0-9])[:]([0-5][0-9])[:][0-5][0-9]$");
	
	public Sol1FlightReader(Element xml_flight) throws FlightMonitorException {
		this.number = xml_flight.getAttribute("number");
		if(!ValidateFlightNumber(this.number)) throw new FlightMonitorException("Invalid flight number " + this.number);
		
		//deptIATAcode
		NodeList nodeList =  xml_flight.getElementsByTagName("deptIATAcode");
		Node node = nodeList.item(0).getFirstChild();
		if(node == null)
			throw new FlightMonitorException("Sol1FlightReader null ref, necessary element not present in XML doc");
		this.deptIATAcode = node.getNodeValue();
		if(!ValidateIATACode(this.deptIATAcode)) 
			throw new FlightMonitorException("Invalid deptIATAcode for flight " + this.number);
		
		//destIATAcode
		nodeList = xml_flight.getElementsByTagName("destIATAcode");
		node = nodeList.item(0).getFirstChild();
		if(node == null)
			throw new FlightMonitorException("Sol1FlightReader null ref, necessary element not present in XML doc");
		this.destIATAcode = node.getNodeValue();
		if(!ValidateIATACode(this.destIATAcode)) 
			throw new FlightMonitorException("Invalid destIATAcode for flight " + this.number);
		
		//deptTime
		nodeList = xml_flight.getElementsByTagName("deptTime");
		node = nodeList.item(0).getFirstChild();
		if(node == null)
			throw new FlightMonitorException("Sol1FlightReader null ref, necessary element not present in XML doc");
		this.deptTime = StringToTime(node.getNodeValue());
	}
	
	public Time StringToTime(String str_time) throws FlightMonitorException{
		try{
			Matcher matcher = timePattern.matcher(str_time);
			if(!matcher.matches()) 
				throw new FlightMonitorException("Invalid deptTime for flight" + this.number);
			int hh = Integer.parseInt(matcher.group(1));
			int mm = Integer.parseInt(matcher.group(2));
			Time tmpTime = new Time(hh, mm);
			
			return tmpTime;
		}
		catch(NumberFormatException e){ //number parsing failed
			throw new FlightMonitorException("Invalide deptTime for flight " + this.number);
		}
		catch (IllegalArgumentException e) { //time failed
			throw new FlightMonitorException("Invalide deptTime for flight " + this.number);
		}
	}
	
	public static boolean ValidateIATACode(String IATAcode){
		if(IATAcode.matches("^[A-Za-z]{3}$")) return true;
		else return false;
	}

	public static boolean ValidateFlightNumber(String flightNumber){
		if(flightNumber.matches("^[A-Za-z]{2}[0-9]{1,4}$")) return true;
		else return false;
	}
	
	@Override
	public String getDepartureAirport() {
		return this.deptIATAcode;
	}

	@Override
	public Time getDepartureTime() {
		return this.deptTime;
	}

	@Override
	public String getDestinationAirport() {
		return this.destIATAcode;
	}

	@Override
	public String getNumber() {
		return this.number;
	}

}
