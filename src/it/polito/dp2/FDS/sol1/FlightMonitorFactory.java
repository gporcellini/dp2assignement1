package it.polito.dp2.FDS.sol1;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.List;

import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;

import it.polito.dp2.FDS.FlightInstanceReader;
import it.polito.dp2.FDS.FlightMonitor;
import it.polito.dp2.FDS.FlightMonitorException;
import it.polito.dp2.FDS.MalformedArgumentException;
import it.polito.dp2.FDS.PassengerReader;

public class FlightMonitorFactory extends it.polito.dp2.FDS.FlightMonitorFactory  {
	@Override
	public FlightMonitor newFlightMonitor() throws FlightMonitorException {
		Sol1FlightMonitor monitor = Sol1FlightMonitor.getInstance();
		
		monitor.parse(System.getProperty("it.polito.dp2.FDS.sol1.FlightInfo.file"));
		
		return monitor;
	}
	
	public static void main(String[] args){
		if(args.length != 2) {
			System.out.println("Missing Argument");
			return;
		}
		
		System.setProperty("it.polito.dp2.FDS.sol1.FlightInfo.file", args[0]);
		FlightMonitorFactory factory = new FlightMonitorFactory();
		try {
			FlightMonitor monitor = factory.newFlightMonitor();
			FDSInfoSerializer f = new FDSInfoSerializer(monitor);
			f.printAll();
			List<FlightInstanceReader> flightInstances;
			try {
				FlightInstanceReader flightInstanceReader2 =
						monitor.getFlightInstance("AB123", new GregorianCalendar(2015, 0, 24));
				if(flightInstanceReader2 != null){
					System.out.println(
							flightInstanceReader2.getFlight().getNumber() + " " +
							new SimpleDateFormat("dd/MM/yyyy").format(flightInstanceReader2.getDate().getTime()) + " " +
							flightInstanceReader2.getStatus().toString()
							);
				} else {
					System.out.println("ok");
				}
				flightInstances = monitor.getFlightInstances("YC285", new GregorianCalendar(2010, 2-1, 16), null);
				for (FlightInstanceReader flightInstanceReader : flightInstances) {
					System.out.println(
							flightInstanceReader.getFlight().getNumber() + " " +
							new SimpleDateFormat("dd/MM/yyyy").format(flightInstanceReader.getDate().getTime()) + " " +
							flightInstanceReader.getStatus().toString()
							);
				}
			} catch (MalformedArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			f.serialize(new PrintStream(new File(args[1])));
		} catch (FlightMonitorException e) {
			System.err.println("Could not instantiate flight monitor object");
			e.printStackTrace();
			System.exit(1);
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
			System.exit(1);
		} catch (TransformerException e) {
			e.printStackTrace();
			System.exit(1);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.exit(1);
		}
		
		
	}

}
