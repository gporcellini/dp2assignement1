package it.polito.dp2.FDS.tests;

import java.util.*;

import it.polito.dp2.FDS.Time;

public class SimpleFlightReader implements it.polito.dp2.FDS.FlightReader {
	private String departureAirport, destinationAirport, number;
	private Time departureTime;
	
	private LinkedList<SimpleFlightInstanceReader> flightInstances = new LinkedList<SimpleFlightInstanceReader>();
	
	public SimpleFlightReader(String departureAirport, Time departureTime, String destinationAirport, String number) {
		this.departureAirport = departureAirport;
		this.departureTime = departureTime;
		this.destinationAirport = destinationAirport;
		this.number = number;
	}
	
	@Override
	public String getDepartureAirport() {
		return departureAirport;
	}

	@Override
	public Time getDepartureTime() {
		return departureTime;
	}

	@Override
	public String getDestinationAirport() {
		return destinationAirport;
	}

	@Override
	public String getNumber() {
		return number;
	}
	
	public LinkedList<SimpleFlightInstanceReader> getFlightInstances() {
		return flightInstances;
	}
	
	public void addFlightInstance(SimpleFlightInstanceReader flightInstance) {
		flightInstances.add(flightInstance);
	}
}
