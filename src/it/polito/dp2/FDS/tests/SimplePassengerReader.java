package it.polito.dp2.FDS.tests;

public class SimplePassengerReader implements it.polito.dp2.FDS.PassengerReader {
	private boolean boarded;
	private SimpleFlightInstanceReader flightInstance;
	private String name;
	private String seat;
	
	public SimplePassengerReader(boolean boarded, SimpleFlightInstanceReader flightInstance, String name, String seat) {
		this.boarded = boarded;
		this.flightInstance = flightInstance;
		this.name = name;
		this.seat = seat;
	}
	
	@Override
	public boolean boarded() {
		return boarded;
	}

	@Override
	public SimpleFlightInstanceReader getFlightInstance() {
		return flightInstance;
	}

	@Override
	public String getName() {
		return name;
	}
	
	@Override
	public String getSeat() {
		return seat;
	}
}
