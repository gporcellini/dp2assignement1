package it.polito.dp2.FDS.tests;

import it.polito.dp2.FDS.*;

import java.util.*;

public class SimpleFlightInstanceReader implements it.polito.dp2.FDS.FlightInstanceReader {
	private Aircraft aircraft;
	private GregorianCalendar date;
	private int delay;
	private String departureGate;
	private SimpleFlightReader flight;
	private FlightInstanceStatus flightInstanceStatus;
	
	private HashSet<SimplePassengerReader> passengers = new HashSet<SimplePassengerReader>();
	
	public SimpleFlightInstanceReader(Aircraft aircraft, GregorianCalendar date, int delay, String departureGate, SimpleFlightReader flight, FlightInstanceStatus flightInstanceStatus) {
		this.aircraft = aircraft;
		this.date = date;
		this.delay = delay;
		this.departureGate = departureGate;
		this.flight = flight;
		this.flightInstanceStatus = flightInstanceStatus;
	}
	
	@Override
	public Aircraft getAircraft() {
		return aircraft;
	}

	@Override
	public GregorianCalendar getDate() {
		return date;
	}

	@Override
	public int getDelay() {
		return delay;
	}

	@Override
	public String getDepartureGate() {
		return departureGate;
	}

	@Override
	public SimpleFlightReader getFlight() {
		return flight;
	}

	@Override
	public HashSet<PassengerReader> getPassengerReaders(String namePrefix) {
		if (namePrefix != null)
			throw new UnsupportedOperationException();
		return new HashSet<PassengerReader>(passengers);
	}

	@Override
	public FlightInstanceStatus getStatus() {
		return flightInstanceStatus;
	}
	
	public void addPassenger(SimplePassengerReader passenger) {
		passengers.add(passenger);
	}
}
